package main

import (
	"fmt"
	"net/http"

	"github.com/SlyMarbo/rss"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/nyt", func(c *gin.Context) {
		feed, err := rss.Fetch("https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml")
		if err != nil {
			// handle error.
			fmt.Println("Error Occurred")
		}

		c.JSON(http.StatusOK, feed.Items)
	})
	r.GET("/toi", func(c *gin.Context) {
		feed, err := rss.Fetch("https://timesofindia.indiatimes.com/rssfeedstopstories.cms")
		if err != nil {
			// handle error.
			fmt.Println("Error Occurred")
		}

		c.JSON(http.StatusOK, feed.Items)
	})
	r.GET("/google-news", func(c *gin.Context) {
		feed, err := rss.Fetch("https://news.google.com/rss?hl=en-US&gl=US&ceid=US:en")
		if err != nil {
			// handle error.
			fmt.Println("Error Occurred")
		}
		c.JSON(http.StatusOK, feed.Items)
	})
	r.GET("/bbc", func(c *gin.Context) {
		feed, err := rss.Fetch("https://feeds.bbci.co.uk/news/rss.xml")

		// for _, item := range feed.Items {
		// 	fmt.Println(item)
		// }
		if err != nil {
			// handle error.
			fmt.Println("Error Occurred")
		}
		c.JSON(http.StatusOK, feed.Items)
	})
	r.GET("/techcrunch", func(c *gin.Context) {
		feed, err := rss.Fetch("https://techcrunch.com/feed/")

		// for _, item := range feed.Items {

		// 	fmt.Println(item)
		// }
		if err != nil {
			// handle error.
			fmt.Println("Error Occurred")
		}
		c.JSON(http.StatusOK, feed.Items)
	})

	r.Run()
}
